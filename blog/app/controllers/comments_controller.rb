class CommentsController < ApplicationController
  def create
    @post = Post.find(params[:post_id])
    @comment = @post.comments.create(comments_parameters)
    redirect_to post_path(@post)
  end

  private def comments_parameters
    params.require(:comment).permit(:username, :body)
  end

end
