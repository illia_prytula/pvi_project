class PostsController < ApplicationController
before_action :authenticate_user!, except: [:show, :index]
  def index

    if params[:category_handle]
      @category = Category.find_by_handle!(handle: params[:category_handle])

      @posts = @category.posts.ordered.by_categories
    end
    @posts ||= Post.ordered.by_categories
  end

  def show
    @post = Post.find(params[ :id])
  end

  def new
    @post = Post.new
  end

  def create
    @post = Post.new(post_params)
    @post.user = current_user
    if(@post.save)
      redirect_to @post
    else
      render 'new'
    end
  end

  def edit
    @post = Post.find(params[:id])
  end

  def update
    @post = Post.find(params[:id])
    if(@post.update(post_params))
      redirect_to :post
    else
      render 'edit'
    end
  end

  def delete
    @post = Post.find(params[:id])
    @post.destroy
    redirect_to :posts
  end

  private def post_params
    params.require(:post).permit(:title, :text, :category_id)
  end
end
